from Utils.Attack import AttackCheck
import keyboard
import time
import random
import mouse
from numpy import mod


class CharController:
    def __init__(self) -> None:
        self.set('maldraxxus')
        self.moving = False
        self.pause = False
        self.attackCheck = AttackCheck()


    def set(self,map):
        file = open(f'src/{map}.press')
        for line in file.readlines():
            line = line.split('==')
            key=line[0]
            value=eval(line[1])
            setattr(self,key.strip(),value)
        file.close()

                    
    def move(self,mode=True):
        if mode:
            if not self.moving:
                keyboard.press('w')
                self.moving = True
        else:
            if self.moving:
                keyboard.press_and_release('w')
                self.moving = False


    def dMove(self,direction:str,to:str,difrent):
        
        duration = difrent / 2 * self.motion_coefficient
        if direction == 'x':
            if to == 'Left':
                self.keyPress('q',duration)
            else:
                self.keyPress('e',duration)
            pass
        else:
            if to == 'Top':
                self.keyPress('w',duration)
            else:
                self.keyPress('s',duration * 2)


    def keyPress(self,key,duration):
        keyboard.press(key)
        time.sleep(duration)
        keyboard.release(key)

    def rotate(self,side,duration):
        key = 'a' if side == 'left' else 'd'
        self.keyPress(key,duration)


    def saveHimself(self):
        self.rotate(self.attack_rotate[1],self.attack_rotate[0] * 4)
        time.sleep(2)

    def pull(self):
        if self.moving:
            if not self.pause:
                key = random.choice(self.moving_keys)
                if key != None:
                    if key == 'tab':
                        keyboard.press_and_release(key)
                        time.sleep(.6)
                        keyboard.press_and_release(self.moonfire_key)
                    else:
                        keyboard.press_and_release(key)

    def changeForm(self,hide=False):
        time.sleep(.1)
        keyboard.press_and_release(self.travel_key)
        if hide:
            time.sleep(.1)
            self.hideForm()


    def hideForm(self,timing=False):
        if timing:
            time.sleep(timing)
            
        time.sleep(.1)
        keyboard.press_and_release(self.hide_key)
        

    def attack(self,location_number):
        i = 1
        # keyboard.press_and_release('8')
        while(i <= self.attack_time):
            if self.pause == False:
            
                # if i == 1:
                #     keyboard.press_and_release('esc')
                #     time.sleep(.2)
                #     if not self.attackCheck.isAttacking():
                #         break

                
                if self.startAttackChecking != None and i > self.startAttackChecking:    
                    if self.attackCheckTime != None and i % self.attackCheckTime == 0:
                        if not self.attackCheck.isAttacking():
                            rotateStep = int(i / self.every_rotate_time)
                            duration = self.attack_rotate[0] * ( 4 - rotateStep)
                            if duration != 0.0:
                                self.rotate(self.attack_rotate[1],self.attack_rotate[0] * ( 4 - rotateStep))
                            break


                if location_number in self.more_heal[0] and i in self.more_heal[1]:
                    # time.sleep(1.1)
                    # keyboard.press_and_release(self.heal_key)
                    pass
                elif self.heal_times != None and i in self.heal_times:
                    time.sleep(1.1)
                    keyboard.press_and_release(self.heal_key)
                
                
                # if i == self.every_time_boost_heal_use:
                #     keyboard.press_and_release(self.boost_heal_key)
               
            
                if not self.every_jump_time == None and i % self.every_jump_time == 0:
                    keyboard.press_and_release('space')
                
                key = random.choice(self.attack_keys)
                keyboard.press_and_release(key)
                
                if i < self.max_moonfire_time :
                    keyboard.press_and_release('tab')
                    time.sleep(.2)
                    keyboard.press_and_release(self.moonfire_key)
                    
                if i % self.every_time_spell_use == 0:
                    keyboard.press_and_release(random.choice(self.random_spell))
                    
                if i % self.every_rotate_time == 0: 
                    self.rotate(self.attack_rotate[1],self.attack_rotate[0])

                i += 1    
                time.sleep(1)
            else: break
            

            
    def loot_skin(self, screenWidth, screenHeight, herb=False): 
        if herb:
            file = open('src/herb.loot','r')    
        else:
            file = open('src/mouse.loot','r')    
        mouse_pos = []

        for line in file.readlines():
            mouse_pos.append(eval(line))

        if not herb:
            random.shuffle(mouse_pos)

        time.sleep(.5)
        for (x, y) in mouse_pos:
            if self.pause == False:
                xx = int((x*screenWidth)/1440)
                yy = int((y*screenHeight)/900)
                
                if not herb:
                    if mouse_pos.index((x, y)) % self.every_loot_attack_time == 0:
                        keyboard.press_and_release('3')

                mouse.move(xx,yy,duration=.1)
                mouse.right_click()
                time.sleep(.2)
            else: return True

        return True




    
# app = CharController()
# app.move(False)

