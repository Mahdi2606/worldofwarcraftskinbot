
from PIL import ImageGrab
import cv2
import numpy as np
from Utils.Utils import ConvertColor, Utils
import time

class DeadCheck:
    def __init__(self,screenWidth,screenHeight) -> None:
        self.screenWidth,self.screenHeight = screenWidth,screenHeight
        self.deadTemp = cv2.imread('assets/deadalarm.png',0)


    def isDead(self):
        img = np.array(ImageGrab.grab(bbox=(
            int((self.screenWidth / 10)*3.5),
            int((self.screenHeight / 10) *.8),
            int((self.screenWidth / 10)*6.5),
            int((self.screenHeight / 10)*3.5),
        )))
                
        img = ConvertColor.toGray(img)
        match = Utils.match_template(img,self.deadTemp,cv2.TM_SQDIFF_NORMED,.8)
        
        if match:
            return True
        else:
            return False
