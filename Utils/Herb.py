
import os
import sys
from CharControll import CharController
from PIL import ImageGrab
import numpy as np
import cv2
from Utils.Utils import ConvertColor, Utils


class HerbLooking:
    def __init__(self,screenWidth,screenHeight,charController:CharController) -> None:
        self.screenWidth, self.screenHeight, self.charController = screenWidth, screenHeight, charController
        self.dot = cv2.imread('assets/dot.png')
        self.herbCursor = cv2.imread('assets/herbcursor.png')

    def crop(self):
        captcher = ImageGrab.grab(
            bbox=(
                int((self.screenWidth/2)-64),
                int((self.screenHeight/2)-64),
                int((self.screenWidth/2)+64),
                int((self.screenHeight/2)+64)
                )
            )
        
        crop = np.array(captcher)
        crop = ConvertColor.toRgb(crop)
        return crop

    def cropCursorArea(self):
        captcher = ImageGrab.grab(
            bbox=(
                int((self.screenWidth/2)-200),
                int((self.screenHeight/2)-200),
                int((self.screenWidth/2)+200),
                int((self.screenHeight/2)+200)
                )
            )
        
        crop = np.array(captcher)
        crop = ConvertColor.toGray(crop)
        return crop

    def findHerbCursor(self):
        frame = self.cropCursorArea()
        match = Utils.match_template(frame,self.herbCursor,cv2.TM_SQDIFF_NORMED, .15)
        if match:
            return Utils.getMousePostion()
        else:
            return False


    def findPosition(self, frame, x, y,):
        frameHeight, frameWidth = frame.shape[:-1]
        centerX = int(frameWidth/2)
        centerY = int(frameHeight/2)
        dotHeight, dotWidth = self.dot.shape[:-1]
        CenterXDot = int(dotWidth/2)
        CenterYDot = int(dotHeight/2)
        x += CenterXDot
        y += CenterYDot
        side = []
        difrent = []
        if abs(centerX-x) > 0:
            if y < centerY:
                side.append('Top')
            else:
                side.append('Bottom')
        else:
            side.append('Center')

        if abs(centerX-x) > 0:
            if x < centerX:
                side.append('Left')
            else: 
                side.append('Right')
        else:
            side.append('Center')

        difrent.append(abs(centerX-x))
        difrent.append(abs(centerY-y))

        return (side,difrent)


    def adjust(self,side:list,difrent:list):
        y, x = side
        if x != 'Center':
            self.charController.dMove(direction='x',to=x,difrent=difrent[0])
        if y != 'Center':
            self.charController.dMove(direction='y',to=y,difrent=difrent[1])
            
        # print(difrent)
        return True


    def look(self):
        try:
            frame = self.crop()
        
            # cv2.imshow('frame',frame)
            # cv2.waitKey(0)
            # cv2.destroyAllWindows()
            match = Utils.match_template(frame, self.dot, cv2.TM_SQDIFF_NORMED, .15)
            if match:
                x,y = match
                side,difrent = self.findPosition(frame, x, y)
                adjust = self.adjust(side,difrent)
                
                return x,y
            
            else:
                return False
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            Utils.addLog(e,fname, exc_tb.tb_lineno)
