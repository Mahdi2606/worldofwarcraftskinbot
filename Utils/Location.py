
import mouse
import keyboard
from pynput.keyboard import Controller


class Location:
    def __init__(self) -> None:
        pass

    staticmethod

    def readLocations(map):
        locationsFile = open(f'src/{map}.loc', 'r')
        locationPoints = []

        for line in locationsFile.readlines():
            x, y, attack, movingAttak, form, herb = eval(line)
            locationPoints.append((x, y, attack, movingAttak, form, herb))
        locationsFile.close()
        return locationPoints

    staticmethod

    def setLocation(screenWidth, screenHeight, controller, point, first=False):
        Xcenter = screenWidth/2
        Ycenter = screenHeight/2
        if first:
            speed = .1
        else:
            speed = 0
        mouse.move(Xcenter, 0, duration=speed)
        if first:
            mouse.click()
        else:
            mouse.click()
            mouse.click()

        controller.type(str(point[0]))
        keyboard.press_and_release('tab')
        controller.type(str(point[1]))
        keyboard.press_and_release('enter')
        if first:
            mouse.move(Xcenter, Ycenter + 200, duration=0)
        return True


# keyboardController = Controller()
# points = Location.readLocations('maldraxxus')
# Location.setLocation(1440,900,keyboardController,(27.2,57.1),first=True)
