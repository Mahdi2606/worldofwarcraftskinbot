
import os
import sys
from Utils.Utils import Utils
import time
import configparser
import requests



class AlertManager:

    def __init__(self,username,alertIdList) -> None:
        self.username = username
        self.adminIdList = alertIdList


    def getTelegramToken(self):
        try:
            parser = configparser.RawConfigParser()
            parser.read('config.cfg')
            telegramToken = str(parser.get('tel-config', 'tt'))
            return telegramToken
        except Exception as e:
            return ''

    def changeUsername(self,username):
        self.username = username

    def getStatus(self,mode,location=False,bySelf=False):
        
        if mode == 'start':
            status = ['Started','✅']

        elif mode == 'stop':
            status = ['Stoped', '⚠️']
            
        
        elif mode == 'disconnect':
            status = ['Disconnected' , '❌']

        elif mode == 'dead':
            status = ['is Dead' , '☠️']

        elif mode == 'resume':
            status = ['Resumed' , '▶️']

        else:
            status = ['Closed','⛔️']


        if bySelf and mode =='stop':
            status = ['Paused', '⏸']
        else:
            if location and mode == 'stop' or mode == 'dead':
                status.append(f" in location {location}")



        return status


        
    def telegram_alert(self,mode,location=False,bySelf=False,timing=False):
        try:
            token = self.getTelegramToken()
            status = self.getStatus(mode,location,bySelf)
            text = f'''{status[1]} {self.username.capitalize()} {status[0]} {status[2] if len(status) > 2 else ''}'''
            t = f'''🕔 Time: {time.strftime('%H:%M:%S')}'''
            msg = ''
            msg += ' \n'
            msg += f'{text}\n'
            msg += f'{t}\n'
            msg += ' \n'

            if timing:
                time.sleep(timing)

            for i in self.adminIdList:
                url = f'''https://api.telegram.org/bot{token}/sendMessage?chat_id={i}&parse_mode=markdown&text={msg}'''
                response = requests.get(url)
                # print(response.json())
            return True
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            Utils.addLog(e,fname, exc_tb.tb_lineno)
            return False



