import os.path
import configparser
# import win32gui
import ctypes
import time
from urllib.request import urlopen
import cv2
import keyboard
import mouse
import numpy as np
from pynput.keyboard import Controller as KeyController
import winreg
from random import randint


class Location:
    classmethod
    def get_location_box(w, h):
        screeWidthCenter = w/2
        boxXstart = int(screeWidthCenter - ((w/8)*.7))
        boxXend = int(screeWidthCenter + ((w/8)*.7))
        boxYstart = 0
        boxYend = int(h/10)
        boxPostion = [(boxXstart, boxYstart), (boxXend, boxYend)]

        return boxPostion

    def crop_location_box(frame, top_left, bottom_right):
        crop = frame[top_left[1]:bottom_right[1], top_left[0]:bottom_right[0]]
        return crop


class ConvertColor:

    staticmethod
    def toGray(frame):
        return cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    
    staticmethod
    def toRgb(frame):
        return cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)



class Utils:

    # classmethod
    # def checkKeyboardLang():
    #     user32 = ctypes.WinDLL('user32', use_last_error=True)
    #     curr_window = user32.GetForegroundWindow()
    #     thread_id = user32.GetWindowThreadProcessId(curr_window, 0)
    #     klid = user32.GetKeyboardLayout(thread_id) 
    #     lid = klid & (2**16 - 1)
    #     lid_hex = hex(lid)
    #     #for see all language LCID get http://atpad.sourceforge.net/languages-ids.txt
    #     return lid_hex

    # classmethod
    # def changeLanguageToEN():
    #     while True:
    #         langID = Utils.checkKeyboardLang()
    #         print(langID)
    #         if langID != '0x409':
    #             keyboard.press_and_release('alt+shift')
    #             time.sleep(.3)
    #         else:
    #             break
    #     return True


    classmethod
    # def focuseOnWarcraft():
    #     toplist = []
    #     winlist = []
    #     def enum_callback(hwnd, results):
    #         winlist.append((hwnd, win32gui.GetWindowText(hwnd)))

    #     win32gui.EnumWindows(enum_callback, toplist)
    #     warcraft = [(hwnd, title) for hwnd, title in winlist if 'World of Warcraft' in title][0][0]
    #     win32gui.SetForegroundWindow(warcraft)

    def focuseOnWarcraft(screenWidth,screenHeigh):
        mouse.move(int(screenWidth/2),int(screenHeigh/2))
        mouse.click()
        mouse.click()
        time.sleep(.2)

    classmethod
    def getUsernameFromConfig():
        parser = configparser.RawConfigParser()
        parser.read('config.cfg')
        username = ''
        if parser.has_section('bot-config'):
            if parser.has_option('bot-config','username'):
                username = parser.get('bot-config','username')
            else:
                username = f'bot_{randint(10,99)}'
                parser.set('bot-config','username',username)

        else:
            username = f'bot_{randint(10,99)}'
            parser.add_section('bot-config')
            parser.set('bot-config','username',username)
            
        configfile =  open('config.cfg', 'w')
        parser.write(configfile)
        configfile.close()
        return username

    classmethod
    def setUsernameInConfig(username):
        parser = configparser.RawConfigParser()
        parser.read('config.cfg')
        parser.set('bot-config','username',username)
        configfile =  open('config.cfg', 'w')
        parser.write(configfile)
        configfile.close()
        return username


    classmethod
    def resizeWowLogo(tempsiz:tuple,screenSize:tuple,baseSize:tuple):
        
        widthScale = round(screenSize[0] / baseSize[0],2)
        heightRatio = round(tempsiz[1]/tempsiz[0],2)
        
        newWidth = round(tempsiz[0] * widthScale)
        newHeight = round(newWidth * heightRatio)
        
        return (newWidth-1,newHeight-1)



    classmethod
    def match_template(frame, template, method,minthresh,maxthresh = False):
        try:
            res = cv2.matchTemplate(frame,template,method)        
            minThreshold = minthresh
            maxThreshold = minthresh
            if maxthresh == False:
                match_locations = np.where(res<=minThreshold)
            else:
                match_locations = np.where(res<=minThreshold>=maxthresh)
            min_val, max_val, minloc, maxloc = cv2.minMaxLoc(res)
            # print(min_val,max_val)
            if(len(match_locations[1]) != 0 or len(match_locations[0]) != 0 ):
                x, y = (match_locations[1][0], match_locations[0][0])
                return (x,y)
            else : 
                return False
        
        except Exception as e:
            print(e)


    classmethod
    def get_system_uuid():
        registry = winreg.HKEY_LOCAL_MACHINE
        address = 'SOFTWARE\\Microsoft\\Cryptography'
        keyargs = winreg.KEY_READ | winreg.KEY_WOW64_64KEY
        key = winreg.OpenKey(registry, address, 0, keyargs)
        value = winreg.QueryValueEx(key, 'MachineGuid')
        winreg.CloseKey(key)
        unique = value[0]
        return unique
       


    classmethod
    def getAlertIdList():
        try:
            file = open('alert.tel','r')
            idList = [565079277,464473461,122452400]
            for line in file.readlines():
                idList.append(int(line))
            return idList
        except Exception as e:
            return [565079277,464473461,122452400]

    classmethod  
    def findLocaitonsFiles() -> dict:
        import os
        ardenweald = []
        maldraxxus = []
        for root,dir,files in os.walk("./src/", topdown=False):
            for name in files:
                if 'ardenweald' in name and '.loc' in name:
                    ardenweald.append(name)

                elif 'maldraxxus' in name and '.loc' in name:
                    maldraxxus.append(name)
                    
        return {
            "ardenweald" : ardenweald,
            "maldraxxus" : maldraxxus
        }


    classmethod
    def addLog(e,filename,line):
        t = time.strftime('%Y/%m/%d-%H:%M:%S')
        file = open(os.path.dirname(__file__) + '/../.log', 'a')
        file.write(f'---------------------{t} File:{filename} in line:{line}---------------------\n{e}\n')
        file.close()


    
    classmethod
    def getMousePostion():
        return mouse.get_position()
