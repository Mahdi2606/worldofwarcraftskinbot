

from PIL import ImageGrab
import cv2
import numpy as np
from Utils.Utils import ConvertColor, Utils


class ConnectionCheck:
    def __init__(self,screenWidth,screenHeight) -> None:
        self.screenWidth,self.screenHeight = screenWidth,screenHeight
        self.disconnectTemplate = cv2.imread('assets/wow.png',0)


    def isDisconnect(self):
        img = np.array(ImageGrab.grab(bbox=(
            0,
            0,
            int((self.screenWidth / 10) * 2.5),
            int((self.screenHeight / 10) * 2.5)
        )))

        img = ConvertColor.toGray(img)
        h, w = self.disconnectTemplate.shape
        responsiveLogo = Utils.resizeWowLogo((w,h),(self.screenWidth,self.screenHeight),(1440,900))
        rw, rh = responsiveLogo
        rdisconnectTemplate = cv2.resize(self.disconnectTemplate,(rw,rh))
        match = Utils.match_template(img,rdisconnectTemplate,cv2.TM_SQDIFF_NORMED,.1)
        if match:
            return True
        else:
            return False