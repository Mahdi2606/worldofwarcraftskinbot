import os
import sys
from Utils.Disconnect import ConnectionCheck
from Utils.Dead import DeadCheck
from Utils.Herb import HerbLooking
import time
import cv2
import numpy as np
from pynput.keyboard import Controller
from PIL import ImageGrab
from pyautogui import size
from CharControll import CharController

# Utils
from Utils.Utils import ConvertColor, Utils
from Utils.Location import Location
from Utils.CustomThread import Thread
from Utils.AlertManager import AlertManager


class Robot:
    def __init__(self, ui) -> None:

        # data from config
        self.username = Utils.getUsernameFromConfig()
        self.alertIdList = Utils.getAlertIdList()

        # set Screen size
        self.screenWidth, self.screenHeight = size()

        # access Controller Ui
        self.ui = ui

        # controllers
        self.charController = CharController()
        self.keyboardController = Controller()
        self.alert = AlertManager(self.username, self.alertIdList)
        self.DeadCheck = DeadCheck(self.screenWidth, self.screenHeight)
        self.ConnectionCheck = ConnectionCheck(
            self.screenWidth, self.screenHeight)

        # herb
        self.herbLooking = HerbLooking(
            self.screenWidth, self.screenHeight, self.charController)

        # templates
        self.locatioPointTemplate = cv2.imread('assets/point2.png', 0)

        # properties
        self.map = 'maldraxxus'
        self.pause = True
        self.exit = False
        self.lookUpExit = False
        self.lookUpStatus = False
        self.mapPoints = []
        self.currentPointIndex = 0
        self.currentPointGeo = ()
        self.attack = False
        self.firstRun = True
        self.moving = False
        self.pull = False
        self.form = False
        self.herb = False
        self.teravelForm = False
        self.pointInCenter = False
        self.earlytime = None
        self.firstStop = True
        # threads
        self.deadThread = Thread(target=self.checkDead)
        self.connectionThread = Thread(target=self.checkConnection)

    def run(self):
        self.mapPoints = Location.readLocations(self.map)
        self.setPoint(True)
        Thread(target=lambda: self.alert.telegram_alert('start')).start()
        self.firstRun = False
        self.deadThread.start()
        self.connectionThread.start()
        self.pointManage()

    def closeApp(self):
        if not self.firstRun:
            Thread(target=lambda: self.alert.telegram_alert(
                'close', location=self.currentPointGeo)).start()
            self.deadThread.terminate()
            self.connectionThread.terminate()

        self.move(False)
        self.lookUpExit = True
        self.charController.pause = True
        self.exit = True

    def pauseApp(self, bySelf=True, mode=False):
        Utils.focuseOnWarcraft(self.screenWidth,self.screenHeight)
        self.move(False)
        if not self.pause:
            if bySelf:
                Thread(target=lambda: self.alert.telegram_alert(
                    'stop', bySelf=True)).start()
            else:
                if not mode:
                    Thread(target=lambda: self.alert.telegram_alert(
                        'stop', location=self.currentPointGeo)).start()
                else:
                    Thread(target=lambda: self.alert.telegram_alert(
                        mode, location=self.currentPointGeo, timing=60 if mode == 'disconnect' else False)).start()

        self.lookUpExit = True
        self.charController.pause = True
        self.pause = True
        self.ui.resume_btn.setDisabled(False)
        self.ui.pause_btn.setDisabled(True)
        self.ui.reset_btn.setDisabled(False)


    def resumeApp(self):

        findPoint = self.findPoint()
        if not findPoint[0]:
            self.setPoint()

        Utils.focuseOnWarcraft(self.screenWidth, self.screenHeight)
        Thread(target=lambda: self.alert.telegram_alert('resume')).start()
              
        self.lookUpExit = False
        self.charController.pause = False
        self.pause = False

        

    # check dead
    def checkDead(self):
        while True:
            i = 0
            while i < 10:
                time.sleep(1)
                i += 1
            if not self.pause:
                if self.DeadCheck.isDead():
                    self.pauseApp(mode='dead', bySelf=False)

    # check Connection

    def checkConnection(self):
        while True:
            i = 0
            while i < 10:
                time.sleep(1)
                i += 1
            if not self.pause:
                if self.ConnectionCheck.isDisconnect():
                    self.pauseApp(mode='disconnect', bySelf=False)

    def setMap(self, map: str):
        self.map = map
        self.charController.set(self.map)

    def setUsername(self, username):
        self.username = username
        self.alert.changeUsername(username)

    def move(self, mode=True):
        self.charController.move(mode)
        if mode:
            self.moving = True
        else:
            self.moving = False

    def setPoint(self, first=False):
        currentPoint = self.mapPoints[self.currentPointIndex]
        self.currentPointGeo = currentPoint[:2]
        self.attack = currentPoint[2]
        self.pull = currentPoint[3]
        self.form = currentPoint[4]
        self.herb = currentPoint[5]
        Location.setLocation(self.screenWidth, self.screenHeight,
                             self.keyboardController, self.currentPointGeo, first)
        self.currentPointIndex += 1

        time.sleep(.1)

    def findPoint(self):

        # grab screen
        captcher = ImageGrab.grab(
            bbox=(
                int((self.screenWidth/2)-(self.screenWidth/10)*2),
                int((self.screenHeight/2)-(self.screenHeight/10)*3.5),
                int((self.screenWidth/2)+(self.screenWidth/10)*2),
                int((self.screenHeight/2)+(self.screenHeight/10)*3.5)
            )
        )

        # read frame of video
        frame = np.array(captcher)
        # chane frame color to grayscale
        frame = ConvertColor.toGray(frame)

        # crop a box in the center of the frame
        centerBox = frame

        # get width and height of center box
        CenterBoxHeight, CenterBoxWidth = centerBox.shape

        # match template
        match_locations = Utils.match_template(
            centerBox, self.locatioPointTemplate, cv2.TM_SQDIFF_NORMED, 0.36)

        return [match_locations, (CenterBoxHeight, CenterBoxWidth)]

    def lookUp(self):
        self.earlytime = int(time.time())

        # get width and height of location template
        locationPointTemplateWidth, locationPointTemplateHeight = self.locatioPointTemplate.shape

        while True:
            # Exit Bot
            if self.lookUpExit:
                break

            # find point
            findPoint = self.findPoint()

            # get width and height of center box
            CenterBoxHeight, CenterBoxWidth = findPoint[1]

            # handle lcation

            match_locations = findPoint[0]
            if match_locations != False:
                # start moving

                if not self.pause:
                    if (int(time.time())-self.earlytime) == 25:
                        if self.firstStop:
                            self.charController.saveHimself()
                            self.firstStop = False
                        else:
                            self.pauseApp(bySelf=False)
                            self.firstStop = True

                if not self.moving :
                    if not self.pause:
                        self.move()
                # if not self.form:
                #     if not self.moving:
                #         self.move()

                # else:
                #     if self.pointInCenter:
                #         self.move()

                x, y = match_locations
                matchX = int(x+(locationPointTemplateWidth/2))
                matchY = int(y+(locationPointTemplateHeight/2))
                centerXS = int((CenterBoxWidth/2) -
                               (locationPointTemplateWidth/2)) - 2
                centerXE = int((CenterBoxWidth/2) +
                               (locationPointTemplateWidth/2)) + 2

                if self.pull:
                    self.charController.pull()

                if matchX > centerXS and matchX < centerXE:
                    if matchY > (CenterBoxHeight/2):
                        if matchX > (CenterBoxWidth/2):
                            self.charController.rotate('right',self.charController.moving_rorate_time)
                        else:
                            self.charController.rotate('left',self.charController.moving_rorate_time)
                    else:
                        self.pointInCenter = True
                else:
                    if matchX < centerXS:
                        self.charController.rotate('left',self.charController.moving_rorate_time)
                    elif matchX > centerXE:
                        self.charController.rotate('right',self.charController.moving_rorate_time)
            else:
                self.pointInCenter = False
                self.lookUpStatus = True
                break
        return self.lookUpStatus

    def pointManage(self):
        try:
            while self.currentPointIndex < len(self.mapPoints)+1:
                # Reset point to first location
                if self.currentPointIndex == len(self.mapPoints):
                    self.currentPointIndex = 0

                if(self.exit):
                    break

                # pause controll
                if not self.pause:
                    # check to need change form
                    if self.form:
                        if not self.teravelForm:
                            self.charController.changeForm(hide=True)
                            self.teravelForm = True
                    else:
                        if self.teravelForm:
                            self.charController.changeForm()
                            self.teravelForm = False

                    # looking for point on the map
                    looking = self.lookUp()

                    if(self.exit):
                        break

                    if looking:
                        if self.herb:
                            if self.moving:
                                self.move(False)

                            time.sleep(.2)
                            find = self.herbLooking.look()
                            if find:
                                self.charController.loot_skin(
                                    self.screenWidth, self.screenHeight, True)

                                if self.mapPoints[self.currentPointIndex][5]:
                                    self.charController.hideForm(self.charController.hideTime)
                                    
                        else:
                            if self.attack:
                                if self.moving:
                                    self.move(False)

                                self.charController.attack(
                                    self.currentPointIndex)
                                self.charController.loot_skin(
                                    self.screenWidth, self.screenHeight)

                        if not self.pause or not self.exit:
                            self.setPoint()
                    else:
                        pass
        except Exception as e:
            print(e)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            Utils.addLog(e, fname, exc_tb.tb_lineno)
