from PIL import ImageGrab
import cv2
import numpy as np
from Utils.Utils import ConvertColor, Utils

class AttackCheck:
    def __init__(self) -> None:
        # self.screenWidth,self.screenHeight = screenWidth,screenHeight
        self.attackTemp = cv2.cv2.imread('./assets/attackMode.png',0)


    def crop(self):

        captcher = ImageGrab.grab(
            bbox=(
                int(0),
                int(0),
                int(300),
                int(300)
                )
            )
        
        crop = np.array(captcher)
        crop = ConvertColor.toGray(crop)
        return crop

    def isAttacking(self):
        frame = self.crop()
        match = Utils.match_template(frame,self.attackTemp,cv2.TM_SQDIFF_NORMED, .3)
        if match:
            x, y = match
            return x, y
        else:
            return False
